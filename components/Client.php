<?php
/**
 * Created by PhpStorm.
 * User: Pavel
 * Date: 02.12.2020
 * Time: 12:50
 */

namespace app\components;


use app\models\Users;
use yii\base\Component;

abstract class Client extends Component
{
    protected $id;
    protected $client;

    public function __construct($id)
    {
        $this->client =  Users::find()->where('id='.$id)->one();
    }

    public function getParent()
    {
        return $this->parent;
    }

    public function getClient(){
        return $this->client;
    }

    public function add($client){ }

    abstract public function operation();
    abstract public function logins();
    abstract public function volumes($start,$end);
    abstract public function level($n=0);
    abstract public function countClientAll();
}