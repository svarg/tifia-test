<?php
/**
 * Created by PhpStorm.
 * User: Pavel
 * Date: 02.12.2020
 * Time: 12:54
 */

namespace app\components;

use app\models\Users;

class ClientParent extends Client
{
    public $children = [];
    protected $level = 0;

    public function add($client)
    {
        $this->children[] = $client;
    }

    public function operation($n = 0)
    {
        $results = [];
        $n++;
        foreach ($this->children as $child) {
            $results[] = $child->operation($n);
        }

        return implode($results);
    }

    public function logins()
    {
        $results = [];
        foreach ($this->children as $child) {
            if (Users::find()->where(['partner_id' => $this->getClient()->client_uid])->count() > 1) {
                $results[$child->getClient()->id] = $child->logins();
            } else {
                $results[] = $child->logins();
            }
        }
        return $results;
    }

    public function volumes($start, $end)
    {
        $sum = 0;
        foreach ($this->children as $child) {
            $row = (new \yii\db\Query())
                ->select(['u.id', 'a.client_uid', 'SUM((t.volume)*(t.coeff_h)*(t.coeff_cr)) vol'])
//                ->select(['u.id as id', 'a.client_uid as client_uid','t.volume as volume','t.coeff_h as coeff_h','t.coeff_cr as coeff_cr'])
                ->from('users u')
                ->leftJoin('accounts a', 'a.client_uid=u.client_uid')
                ->leftJoin('trades t', 't.login=a.login')
                ->where(['between', 't.close_time', $start, $end])
                ->andWhere(['u.id' => $child->getClient()->id])
                ->one();
            $sum = $row->vol;
//            foreach ($rows as $row){
//                $sum += $row['volume']*$row['coeff_h']*$row['coeff_cr'];
//            }
            $sum += $child->volumes($start, $end);
        }
        return $sum;
    }

    public function volumesId($start, $end)
    {
        $row = (new \yii\db\Query())
            ->select(['u.id', 'a.client_uid', 'SUM((t.volume)*(t.coeff_h)*(t.coeff_cr)) vol'])
//                ->select(['u.id as id', 'a.client_uid as client_uid','t.volume as volume','t.coeff_h as coeff_h','t.coeff_cr as coeff_cr'])
            ->from('users u')
            ->leftJoin('accounts a', 'a.client_uid=u.client_uid')
            ->leftJoin('trades t', 't.login=a.login')
            ->where(['between', 't.close_time', $start, $end])
            ->andWhere(['u.id' => $this->getClient()->id])
            ->one();
        $sum = $row->vol;

        return $sum;
    }

    public function level($n = 0)
    {
        $n++;
        foreach ($this->children as $child) {
            $temp = $child->level($n);
        }
        return $temp > $n ? $temp : $n;
    }

    public function countClient()
    {
        return Users::find()->where(['partner_id' => $this->getClient()->client_uid])->count();
    }

    public function countClientAll()
    {
        $count = 0;
        foreach ($this->children as $child) {
            $count = Users::find()->where(['partner_id' => $this->getClient()->client_uid])->count();
            $count += $child->countClientAll();
        }

        return $count;
    }

}