<?php
/**
 * Created by PhpStorm.
 * User: Pavel
 * Date: 02.12.2020
 * Time: 12:53
 */

namespace app\components;

use app\models\Users;

class ClientChild extends Client
{

    public function operation()
    {
        return '|--'.$this->getClient()->id.'<br>';
    }

    public function logins(){
        return $this->getClient()->id;
    }

    public function volumes($start, $end)
    {
        $sum = 0;
        $rows = (new \yii\db\Query())
//            ->select(['u.id', 'a.client_uid','t.volume*t.coeff_h*t.coeff_cr vol'])
            ->select(['u.id as id', 'a.client_uid as client_uid', 't.volume as volume', 't.coeff_h as coeff_h', 't.coeff_cr as coeff_cr'])
            ->from('users u')
            ->leftJoin('accounts a', 'a.client_uid=u.client_uid')
            ->leftJoin('trades t', 't.login=a.login')
            ->where(['between', 't.close_time', $start, $end])
            ->andWhere(['u.id' => $this->getClient()->id])
            ->all();

        foreach ($rows as $row) {
            $sum += $row['volume'] * $row['coeff_h'] * $row['coeff_cr'];
        }
        return $sum;
    }


    public function level($n=0){
        $n++;
        return $n.'<br>';
    }

    public function countClientAll(){
        return Users::find()->where(['partner_id'=>$this->getClient()->client_uid])->count();
    }
}