<?php
/**
 * Created by PhpStorm.
 * User: Pavel
 * Date: 30.11.2020
 * Time: 11:46
 */

namespace app\controllers;

use Yii;
use app\components\ClientChild;
use app\components\ClientParent;
use app\models\Partners;
use app\models\Users;
use yii\web\Controller;

class UsersController extends Controller
{
    public function actionIndex(){
        $this->layout='tifia';
        $client = new ClientParent(123);
        $clients = $this->partners($client);
//        echo  $clients->operation();
        return $this->render('index',['data'=>$clients]);

    }

    function actionVolume(){
        $this->layout='tifia';
        //        SELECT u.id, a.client_uid, t.volume*t.coeff_h*t.coeff_cr vol
        //FROM users u
        //LEFT JOIN accounts a ON a.client_uid=u.client_uid
        //LEFT JOIN trades t ON t.login=a.login
        //WHERE u.id=123
        //        AND t.close_time BETWEEN '2019-03-01' AND '2019-03-31'
        //        ;

        $client = new ClientParent(123);
        $clients = $this->partners($client);
       // $sum = $clients->volumes('2019-03-01','2019-03-02');


//        $accounts = $client->getClient()->getAccounts()->all();
//        $sum = 0;
//        foreach ($accounts as $account){
//          $trades = $account->getTradesBetween('2019-03-01','2019-03-31');
//            foreach ($trades as $trade){
//                $sum += $trade->volume*$trade->coeff_h*$trade->coeff_cr;
//            }
//        }

//        $rows = (new \yii\db\Query())
////            ->select(['u.id', 'a.client_uid','t.volume*t.coeff_h*t.coeff_cr vol'])
//            ->select(['u.id as id', 'a.client_uid as client_uid','t.volume as volume','t.coeff_h as coeff_h','t.coeff_cr as coeff_cr'])
//            ->from('users u')
//            ->leftJoin('accounts a','a.client_uid=u.client_uid')
//            ->leftJoin('trades t','t.login=a.login')
//            ->where(['between','t.close_time','2019-03-01','2019-03-31'])
//            ->andWhere(['u.id' => $client->getClient()->id])
//            ->all();
//
//        $sum = 0;
//        foreach ($rows as $row){
//            $sum += $row['volume']*$row['coeff_h']*$row['coeff_cr'];
//        }
        return $this->render('volume',['data'=>$clients]);

    }
    function actionVolumesId(){
        $request = Yii::$app->request;
        $id = $request->get('key');
        $client = new ClientParent($id);
        $start='2019-03-01';
        $end = '2019-03-31';
//        var_dump($client->volumesId($start,$end));
        return  $client->volumesId($start,$end);

    }

    function actionLevel(){
        $this->layout='tifia';
        $client = new ClientParent(123);
        $clients = $this->partners($client);
        return $this->render('level',['data'=>$clients]);
    }

    function actionCount(){
        $this->layout('tifia');
        $client = new ClientParent(123);
        var_dump($client->countClient());
    }

    private  function partners($client){
        $user_temp = $client->getClient();
        $parents = Users::find()->where('partner_id='.$user_temp->client_uid)->all();
        if(!empty($parents)){
            foreach ($parents as $user){
                //var_dump($user->id);
                $client->add($this->partners(new ClientParent($user->id)));
            }
        }else{
            $client->add(new ClientChild($user_temp->id));
        }
        return $client;
    }

//    private function partners($client_uid){
////        $users = [];
//        $clients = new ClientParent(123);
//        foreach (Users::find()->where('partner_id='.$client_uid)->all() as $user){
//            if(!empty(Users::find()->where('partner_id='.$user->client_uid)->all())){
//                //$users[$user->client_uid][] = $this->partners($user->client_uid);
//                $clients->add($this->partners($user->client_uid));
//                $clients->add($this->partners($user->client_uid));
//
//            }else{
//                $users[]=[$user->client_uid] ;
//            }
//        }
//        return $users;
//    }

}