<?php
/**
 * User: Svarg
 * Date: 30.11.2020
 * Time: 11:33
 */

namespace app\models;

use yii\db\ActiveRecord;

class Users extends ActiveRecord
{

    public static function getPartners($id){
        $users = self::find()->where('id='.$id)->all();
        foreach ($users as $user){
            if(!empty($user->partner_id)){
                $ar[] = self::find()->where('id='.$user->partner_id)->all();
            }
        }
        return self::find()->where('id='.$id)->all();

    }
    public function getAccounts(){
        return $this->hasMany(Accounts::className(),['client_uid'=>'client_uid']);
    }

}