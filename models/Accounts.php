<?php
/**
 * Created by PhpStorm.
 * User: Pavel
 * Date: 07.12.2020
 * Time: 8:28
 */

namespace app\models;


use yii\db\ActiveRecord;

class Accounts extends ActiveRecord
{

    public function getUser()
    {
        return $this->hasOne(Users::className(), ['client_uid' => 'client_uid']);
    }

    public function getTrades()
    {
//        return $this->hasMany(Trades::className(),['login'=>'login','between','t.close_time','2019-03-01','2019-03-31']);
        return $this->hasMany(Trades::className(),['login'=>'login']);
    }

    public function getTradesBetween($start,$end){
        return Trades::find()->where(['between','close_time',$start,$end])->andWhere(['login'=>$this->login])->all();
    }

}