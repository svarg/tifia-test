<?php
/**
 * Created by PhpStorm.
 * User: Pavel
 * Date: 30.11.2020
 * Time: 11:54
 */
$this->title = 'ТИФИЯ-test';
?>

<div class="index">
    <div class="jumbotron">
        <h1>посчитать количество прямых рефералов и количество всех рефералов клиента</h1>
    </div>
    <div class="row" >
        <div class="col-sm-4"></div>
        <div class="col-sm-4">
            <?php
            echo $data->operation();
            ?>
        </div>
        <div class="col-sm-4"></div>
    </div>
    <div class="row">
        <div class="col-sm-4"></div>
        <div class="col-sm-4">
            <span>Кол-во прямыми рефералами:</span>
            <?php
            echo $data->countClient();
            ?>
        </div>
        <div class="col-sm-4"></div>
    </div>
    <div class="row">
        <div class="col-sm-4"></div>
        <div class="col-sm-4">
            <span>Кол-во всего:</span>
            <?php
            echo $data->countClientAll();
            ?>
        </div>
        <div class="col-sm-4"></div>
    </div>

</div>
